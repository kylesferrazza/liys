# Long Island Youth Summit Project
This is my project to be submitted to the 2017 Long Island Youth Summit.
[Live Project](https://climatechange.cf/)

TODO
=====
  - [x] jQuery scroll down links
  - [x] Bootstraperize
  - [x] Cool fonts and font sizes
  - [x] sugar
    - [x] spice
      - [x] everything nice
        - [x] Chemical X
  - [x] Link color
  - [x] Highlight color
  - [x] reword, work in Kay's stuff
  - [x] fix extra box in production
  - [ ] make it work on mobile?
