---
layout: parallax
title: Long Island Effects
order: 6
bg: ice-sheets.jpg
font-color: white
---

"Okay, but how will this affect me here on Long Island?", you may be asking yourself.

**It already is.**
Temperatures in our region have been rising dramatically, you may have even noticed it yourself this winter.
We could go from a day with 15&deg;-20&deg; to the next getting up to 55&deg;-60&deg;, in December!
Not only that, but many environmental groups have been reporting a sea level rise at our beaches, roughly .06 inches a year.

This may not seem like much but the rate also increases each year.
In addition, many marine biologists have been noticing abnormal migration patterns in fish and if this trend contains, you may not be able to catch any fish on the island.

The biggest effect this will have is on the hurricanes and storms that have increased on the island in the past couple of years.
These storms will become more frequent and strong in the future unless we are able to reduce the negative effects of climate change and reverse the damage it has already done.

This raises concerns about how to protect communities from storms.
There are a few ways of doing this.
One option many communities go with is to build walls out of sand bags, but these tend to be expensive and difficult to make.

The far more popular option is to create wetlands, dunes or other natural barriers to protect the inland.
These areas help reduce the effects of a storm, and help calm it down.
