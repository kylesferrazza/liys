---
layout: parallax
title: Effects
order: 4
bg: berg.jpg
font-color: white
---
Now, knowing that world is getting hotter on average, what exactly does that mean?
You may be saying "I would like some warmer weather"; but not everyone does.
Polar bears and other arctic creatures depend on ice and are more adapted to colder conditions.
With rising temperatures, more arctic ice is melting, meaning Polar Bears and their arctic friends' environment is rapidly changing and the animals don't have time to adapt to such speedy, drastic changes.
This is causing Polar bears to become endangered.

Arctic animals aren't the only ones that would need to adapt, humans would too.
Now this is where most people would say "But warmer weather would be nice!".
*Unless* it means getting up to dangerous levels of heat in the summer and upwards of 60&deg;F in the winter.
(In February 2017, Central Park in NYC recorded its hottest ever temperature for the month of February, 70&deg;F!!)
Not only will we have wacky temperatures but the precipitation patterns will change.
The south-east will get major amounts of rain leading to flooding.
California will experience droughts due to the lack thereof.
Humans will face rising sea levels (up to 4 feet by 2100!) and hurricanes will become increasingly stronger.

Climate change is especially harmful to less developed countries, as they depend heavily on natural resources and are the least equipped to contend with the drastic climate changes.
Climate change poses an immense burden on both the society and the economy because of damage to property, human health, and infrastructure.
The fields of agriculture, forestry, energy, and tourism have suffered the most negative consequences of climate change. 

