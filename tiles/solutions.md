---
layout: parallax
title: Solutions
order: 5
bg: vertical.jpg
font-color: white
---
It is likely that you have been told to recycle more and reuse items like water bottles or shopping bags.
While that can be helpful, the best thing we can all do is move to renewable forms of energy.
"But I can't control that!" you may exclaim, but you can!

Burning fossils is the main cause of climate change, so by using clean energy sources, such as wind mills, solar energy or nuclear energy (no, nuclear energy is not dangerous, explained [here](http://www.world-nuclear.org/information-library/safety-and-security/safety-of-plants/safety-of-nuclear-power-reactors.aspx)) we can help protect our Earth and reduce air pollution.

So where do you fit? Get involved by contacting your local politicians and city officials and let them know you care about our planet!
Even if you don't, you should still consider helping at home by using reusable items, biking or walking instead of driving for your commute, and switching off electronics when you don't need them on.
(More things you can do to do your part in reducing climate change can be found [here](http://www.greenpeace.org/international/en/campaigns/climate-change/Solutions/What-you-can-do/), and in the list below)
Hopefully after going through this website you are more aware of our climate change situation, and are willing to do your part to keep our planet habitable, not just for you, but for generations to come.

## Other Ways to Help Out
  - Save energy! Replace your standard light bulbs with eco-friendly fluorescent bulbs and be sure to turn them off when you leave the room. Don’t forget to turn off your electronics when you’re  not using them.
  - Instead of taking a car or a plane, take a train or a bus or a bike or walk instead. If you are driving a car, try to drive a car with better mileage, and combine your errands to save time and money. Public transportation and carpools are a great way to reduce your carbon footprint!
  - Insulate you and your home! Purchase windows that will block out/keep in heat and seal any cracks. Don’t turn the heat up too high in the winter, and use fans in the summer instead of blasting the air conditioning. 
  - Conserve water! Fix drips and leaks, install low-pressure shower heads and toilets. Try to shower as quickly as possible, and make sure to turn off water when you’re not using it. Conserving water will reduce carbon emissions. 
  - Change the way you do laundry! Wash your clothing in cold water and hang it up to dry instead of throwing it in a dryer. 
  - Use high efficient appliances! Appliances with energy star ratings use less energy, and will save the environment and save your money. 
  - GREEN POWER! Convince your local power provider to designate a greater percentage of power from renewable resources, whether it be wind, water, coal or solar, or switch to a company that does. 
  - RECYCLE! Recycle any packaging or consumer goods that you can, or purchase items with minimal or recyclable packaging. Any type of plastic bottle can usually be recycled. Electronics can also be recycled in special facilities. 
  - REPURPOSE! Donate clothing to charity, turn old shirts into rags, sell or donate household goods. This will ultimately reduce the amount of waste that ends up in landfills. 
  - Plants are friends! Plant a tree! Choose plants that will thrive in your area and will require minimal water. 
