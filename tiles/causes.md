---
layout: parallax
title: Causes
order: 3
bg: aurora.jpg
font-color: white
---
While many things have been referred to as the cause of climate change, the main contributor is the expansion of the "Greenhouse Effect."
This effect is caused by heat being trapped due to water vapor and carbon dioxide (CO<sub>2</sub>), among other molecules.

It is referred to as the "greenhouse effect" due to the way a glass greenhouse will similarly maintain heat for long periods of time.

Now, you may be wondering; "Where do humans play into this?"
Well, human activity, most prominently since the Industrial revolution when we began to set up more factories and burn more fossil fuels (coal & oil), has led to increasing amounts of CO<sub>2</sub> in the atmosphere.
This increases the greenhouse effect because the CO<sub>2</sub> will trap more heat.
This is where the name "Global Warming" comes from - an increase in the average global temperature occurs because of the greenhouse effect.

<br>*This does not mean every location on Earth is getting hotter!*<br><br>

It just means that on average it's increasing, and at an alarming rate. To see how this could effect you and why people are so concerned about this matter, continue on to the next section.
