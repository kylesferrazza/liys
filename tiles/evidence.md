---
layout: parallax
title: Evidence
order: 2
bg: glacier.jpg
font-color: white
---
By collecting weather data for over 100 years it is almost universally accepted among scientists that climate change is real and something that we should be concerned about. In a 2013 study of peer-reviewed climate change articles, only 2 out of 10,885 denied *man-made* global warming, but they still recognized that climate change was occurring. ([source](http://www.jamespowell.org))

In addition to now thousands of science backed articles, organizations such as NASA are collecting current data such as:
- Global temperatures have risen around 1.7&deg;F since 1880
- Arctic ice decreases 13.3% per decade
- 281 gigatonnes of land ice melt per year
- Sea level rises 3.4 millimeters per year
- Carbon dioxide levels rise 402.25 parts per million per year
- *All data collected from [NASA](https://climate.nasa.gov)*

Now you may be wondering what all those numbers mean but we'll come back to that in a little bit!
