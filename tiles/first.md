---
layout: parallax
title: Climate Change 
order: 1
bg: eagle-ice.jpg
font-color: white
---
Climate change (also known as global warming) describes the increasing surface temperatures of Earth.

There is substantial scientific evidence that climate change is a result of the human use of fossil fuels, which release carbon dioxide and greenhouse gases into the atmosphere.

Unfortunately, a lot of people in today's society still believe climate change is fake but evidence that it has been happening has been present since the 1970s.

It is very important that people know about this problem and can work together to fix it.

<ul class="linkList">
  {% assign pages_list = site.pages | sort:"order" %}
  {% for pg in pages_list %}
  {% if pg.title != "Climate Change" and pg.layout == "parallax" %}
  <li class="nav" onclick="$('body').scrollTo('#{{pg.title | lowercase | slugify}}-block', 400)">{{pg.title}}</li>
  {% endif %}
  {% endfor %}
</ul>
